#pragma once

#include <vector>
#include "../Graph.hpp"
#include "BronKerboschCommon.hpp"
#include "BronKerboschOriginal.hpp"
#include "BronKerboschPivot.hpp"
#include "BronKerboschDegeneracy.hpp"

namespace CliqueProblem
{
	template<typename Strategy>
	CliquesContainer BronKerbosch(Graph const& graph)
	{
		CliquesContainer result;
		VerticesSet R, P, X;
		for (size_t i = 0; i < graph.VerticesCount(); ++i)
			P.insert(i);


		result.StartTimeCounting();
		Strategy::ApplyStrategy(R, P, X, graph, result);
		result.StopTimeCounting();

		return result;
	}
}
