#pragma once

#include "../Graph.hpp"
#include "../CliquesContainer.hpp"

namespace CliqueProblem
{
	inline VerticesSet SetIntersection(VerticesSet const& set1, VerticesSet const& set2)
	{
		VerticesSet result;

		for (auto& v: set1)
			if (set2.find(v) != set2.end())
				result.insert(v);

		return result;
	}
}
