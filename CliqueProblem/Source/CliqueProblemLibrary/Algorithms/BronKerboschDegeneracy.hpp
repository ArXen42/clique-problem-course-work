#pragma once

#include <algorithm>
#include <forward_list>
#include "../Graph.hpp"
#include "BronKerboschCommon.hpp"
#include "BronKerboschPivot.hpp"

namespace CliqueProblem
{
	typedef std::forward_list<size_t> VerticesList;

	struct DegeneracyOrder
	{
		VerticesList OrderedVertices_;
		size_t Degeneracy_;
	};

	/// BronKerboschDegeneracy(G):
	/// 	P = V(G)
	///	R = X = empty
	/// 	for each vertex v in a degeneracy ordering of G:
	///		BronKerboschPivot(R ⋃ {v}, P ⋂ N(v), X ⋂ N(v))
	///		P := P \ {v}
	///		X := X ⋃ {v}
	template<typename PivotSelectionStrategy>
	struct BronKerboschDegeneracy
	{
		static const std::string AlgorithmName() { return "Degeneracy" + PivotSelectionStrategy::StrategyName(); }

		static DegeneracyOrder GetDegeneracyOrder(VerticesSet const& V, Graph const& graph)
		{
			DegeneracyOrder resultOrder;
			VerticesList& resultList = resultOrder.OrderedVertices_;

			size_t verticesCount = V.size();
			std::vector<VerticesList> D(verticesCount);
			std::vector<size_t> degrees(verticesCount);

			for (size_t v = 0; v < verticesCount; ++v)
			{
				size_t degree = graph.AdjacentVertices(v).size();
				degrees.at(v) = degree;
				D.at(degree).push_front(v);
			}

			size_t degeneracy = 0;
			for (size_t i = 0; i < verticesCount; ++i)
			{
				size_t firstNonEmptyDegree = 100;
				for (size_t degreeD = 0; degreeD < D.size(); ++degreeD)
				{
					if (!D.at(degreeD).empty())
					{
						firstNonEmptyDegree = degreeD;
						break;
					}
				}

				if (firstNonEmptyDegree > degeneracy) degeneracy = firstNonEmptyDegree;
				auto& vertices = D.at(firstNonEmptyDegree);

				size_t vertex = vertices.front();
				resultList.push_front(vertex);
				vertices.pop_front();

				auto& neighbours = graph.AdjacentVertices(vertex);

				for (size_t neighbour: neighbours)
					if (std::find(resultList.cbegin(), resultList.cend(), neighbour) == resultList.end())
					{
						size_t& degree = degrees.at(neighbour);

						D.at(degree).remove(neighbour);
						degree--;
						D.at(degree).push_front(neighbour);
					}
			}

			return resultOrder;
		}

		static void ApplyStrategy(VerticesSet& R, VerticesSet& P, VerticesSet& X,
		                          Graph const& graph, CliquesContainer& cliquesContainer)
		{
			auto degeneracyOrder = GetDegeneracyOrder(P, graph);

			for (auto v: degeneracyOrder.OrderedVertices_)
			{
				auto& neighbours = graph.AdjacentVertices(v);

				VerticesSet Pi = SetIntersection(P, neighbours);
				VerticesSet Xi = SetIntersection(X, neighbours);

				R.insert(v);
				BronKerboschPivot<PivotSelectionStrategy>::ApplyStrategy(R, Pi, Xi, graph, cliquesContainer);
				R.erase(v);

				P.erase(v);
				X.insert(v);
			}
		}
	};
}
