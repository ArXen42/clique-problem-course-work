#pragma once

namespace CliqueProblem
{
	/// BronKerboschOriginal(R, P, X):
	/// if P and X are both empty:
	///		report R as a maximal clique
	/// for each vertex v in P:
	///		BronKerboschOriginal(R ⋃ {v}, P ⋂ N(v), X ⋂ N(v))
	///		P := P \ {v}
	///		X := X ⋃ {v}
	struct BronKerboschOriginal
	{
		static const std::string AlgorithmName() { return "Original"; }

		static void ApplyStrategy(VerticesSet& R, VerticesSet& P, VerticesSet& X,
		                          Graph const& graph, CliquesContainer& cliquesContainer)
		{
#if defined(ENABLE_TIME_BOUNDING)
			if (cliquesContainer.AlgorithmRunningTime() > MAX_ALGORITHM_RUNNING_TIME)
			{
				cliquesContainer.TimeLimitExceed_ = true;
				return;
			}
#endif
			if (P.empty() && X.empty())
			{
#if defined(DISABLE_CLIQUES_SAVE)
				++cliquesContainer.NonSavedCliquesFound_;
#else
				VerticesArray clique(R.begin(), R.end());
				cliquesContainer.AddClique(clique);
#endif
				return;
			}

			while (!P.empty())
			{
				size_t v = *P.begin();
				auto& neighbours = graph.AdjacentVertices(v);

				VerticesSet Pi = SetIntersection(P, neighbours);
				VerticesSet Xi = SetIntersection(X, neighbours);

				R.insert(v);
				ApplyStrategy(R, Pi, Xi, graph, cliquesContainer);
				R.erase(v);

				P.erase(v);
				X.insert(v);
			}
		}
	};
}
