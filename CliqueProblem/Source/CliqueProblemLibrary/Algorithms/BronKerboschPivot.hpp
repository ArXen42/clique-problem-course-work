#pragma once

#include "BronKerboschCommon.hpp"
#include "PivotSelectionStrategies.hpp"

namespace CliqueProblem
{
	///BronKerboschPivot(R,P,X):
	///	if P and X are both empty:
	///		report R as a maximal clique
	///	choose a pivot vertex u in P ⋃ X
	///	for each vertex v in P \ N(u):
	///		BronKerboschPivot(R ⋃ {v}, P ⋂ N(v), X ⋂ N(v))
	///		P := P \ {v}
	///		X := X ⋃ {v}
	template<typename PivotSelectionStrategy>
	struct BronKerboschPivot
	{
		static const std::string AlgorithmName() { return "Pivot" + PivotSelectionStrategy::StrategyName(); }

		static void ApplyStrategy(VerticesSet& R, VerticesSet& P, VerticesSet& X, Graph const& graph, CliquesContainer& cliquesContainer)
		{
#if defined(ENABLE_TIME_BOUNDING)
			if (cliquesContainer.AlgorithmRunningTime() > MAX_ALGORITHM_RUNNING_TIME)
			{
				cliquesContainer.TimeLimitExceed_ = true;
				return;
			}
#endif
			if (P.empty() && X.empty())
			{
#if defined(DISABLE_CLIQUES_SAVE)
				++cliquesContainer.NonSavedCliquesFound_;
#else
				VerticesArray clique(R.begin(), R.end());
				cliquesContainer.AddClique(clique);
#endif
				return;
			}

			size_t pivot = PivotSelectionStrategy::GetPivot(P, X, graph);

			auto it = P.begin();
			while (it != P.end())
			{
				size_t v = *it;
				if (!graph.AreVerticesConnected(v, pivot))
				{
					auto& neighbours = graph.AdjacentVertices(v);

					VerticesSet Pi = SetIntersection(P, neighbours);
					VerticesSet Xi = SetIntersection(X, neighbours);

					R.insert(v);
					ApplyStrategy(R, Pi, Xi, graph, cliquesContainer);
					R.erase(v);

					it = P.erase(it);
					X.insert(v);
				} else
					++it;
			}
		}
	};
}
