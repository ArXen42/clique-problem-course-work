#pragma once

#include <assert.h>
#include "BronKerboschCommon.hpp"

namespace CliqueProblem
{
	struct MaxDegreePivotStrategy
	{
		static const std::string StrategyName() { return "GPX"; }

		static size_t GetPivot(VerticesSet& P, VerticesSet& X, Graph const& graph)
		{
			size_t pivot;
			size_t maxDegree = 0;

			auto it = P.begin();
			while (it != X.end())
			{
				if (it == P.end())
					if (X.empty()) break;
					else it = X.begin();

				size_t vertex = *it;
				size_t degree = graph.AdjacentVertices(vertex).size();
				if (degree >= maxDegree)
				{
					pivot = vertex;
					maxDegree = degree;
				}

				it++;
			}

			assert(pivot < graph.VerticesCount());
			return pivot;
		}
	};

	struct RandomPivotStrategy
	{
		static const std::string StrategyName() { return "GRX"; }

		static size_t GetPivot(VerticesSet& P, VerticesSet& X, Graph const& graph)
		{
			size_t verticesCount = P.size() + X.size();
			size_t pos = (rand()%verticesCount);

			VerticesSet::const_iterator it;

			if (pos < P.size())
			{
				it = P.begin();
				std::advance(it, pos);
			} else
			{
				it = X.begin();
				std::advance(it, pos - P.size());
			}

			return *it;
		}
	};
}
