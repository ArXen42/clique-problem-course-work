#include <iostream>
#include <algorithm>
#include "CliquesContainer.hpp"

namespace CliqueProblem
{
	void CliquesContainer::AddClique(VerticesArray const& clique)
	{
		if (clique.size() == 0)
			throw std::invalid_argument("Clique must contain at least one vertex.");

		cliques_.push_back(clique);

		auto& newClique = cliques_.back();
		std::sort(newClique.begin(), newClique.end());
	}

	void CliquesContainer::Print(size_t addition) const
	{
		for (auto& clique: cliques_)
		{
			for (size_t v:clique)
				std::cout << v + addition << " ";
			std::cout << std::endl;
		}
	}

	bool CliquesContainer::operator==(CliquesContainer const& rhs) const
	{
		if (rhs.cliques_.size() != cliques_.size())
			return false;

		auto& rhsCliques = rhs.Cliques();
		for (auto& thisClique: cliques_)
		{
			auto foundIt = std::find(rhsCliques.begin(), rhsCliques.end(), thisClique);
			if (foundIt == rhsCliques.end())
				return false;
		}

		return true;
	}

	std::chrono::high_resolution_clock::duration CliquesContainer::AlgorithmRunningTime() const
	{
		return stopTime_ > startTime_
		       ? stopTime_ - startTime_
		       : std::chrono::high_resolution_clock::now() - startTime_;
	}

	std::chrono::milliseconds CliquesContainer::AlgorithmRunningTimeMilliseconds() const
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(AlgorithmRunningTime());
	}
}
