#pragma once

#include <vector>
#include <chrono>

namespace CliqueProblem
{
	typedef std::vector<size_t> VerticesArray;

	/// Контейнер, содержащий результат работы какого-либо алгоритма поиска максимальных клик.
	/// Вершины внутри одной клики всегда сортированы в порядке возрастания.
	/// Также содержит информацию о времени работы алгоритма.
	class CliquesContainer
	{
	public:
		std::vector<VerticesArray> const& Cliques() const { return cliques_; }
		void Print(size_t addition = 0) const;

		void AddClique(VerticesArray const& clique);
		void Clear() { cliques_.clear(); }

		bool operator==(CliquesContainer const& rhs) const;

	public:
		std::chrono::high_resolution_clock::duration AlgorithmRunningTime() const;
		std::chrono::milliseconds AlgorithmRunningTimeMilliseconds() const;

		void StartTimeCounting() { startTime_ = std::chrono::high_resolution_clock::now(); }
		void StopTimeCounting() { stopTime_ = std::chrono::high_resolution_clock::now(); }

		/// Если сохранение клик отключено, для оценки количества найденных будет использоваться этот счетчик.
		size_t NonSavedCliquesFound_ = 0;
		bool TimeLimitExceed_ = false;

	private:
		std::vector<VerticesArray> cliques_;

		std::chrono::high_resolution_clock::time_point startTime_ = std::chrono::high_resolution_clock::time_point::min();
		std::chrono::high_resolution_clock::time_point stopTime_ = std::chrono::high_resolution_clock::time_point::min();
	};
}