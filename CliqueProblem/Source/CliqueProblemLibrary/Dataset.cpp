#include "Dataset.hpp"
#include <sstream>
#include <assert.h>

namespace CliqueProblem
{
	std::vector<std::string> SplitString(std::string& str)
	{
		std::vector<std::string> splitStr;
		std::istringstream ss(str);
		std::string buff;

		while (ss >> buff)
			splitStr.push_back(buff);

		return splitStr;
	}

	Dataset::Dataset(std::string graphName, size_t verticesCount, std::vector<GraphEdge> const& edges, CliquesContainer const& cliques)
			: GraphName_(graphName),
			  VerticesCount_(verticesCount),
			  Edges_(edges),
			  Cliques_(cliques) {}

	Dataset Dataset::ReadFrom(std::ifstream& stream)
	{
		if (!stream.is_open())
			throw std::invalid_argument("Input stream is not open.");

		size_t verticesCount = 0;
		size_t edgesCount = 0;
		std::vector<GraphEdge> edges;
		CliquesContainer cliques;

		std::string filenameString;
		std::getline(stream, filenameString);
		auto split = SplitString(filenameString);
		std::string graphName = split.at(2);

		bool preambleFound = false;
		while (!stream.eof())
		{
			std::string str;
			std::getline(stream, str);

			if (str.empty())
				continue;

			auto split = SplitString(str);

			if (split.front() == "c")
				continue;

			if (split.front() == "p")
			{
				if (preambleFound)
					throw std::exception();

				preambleFound = true;
				verticesCount = static_cast<size_t>(std::stol(split.at(2)));
				edgesCount = static_cast<size_t>(std::stol(split.at(3)));

			} else if (split.front() == "e")
			{
				GraphEdge edge(std::stol(split.at(1)) - 1, std::stol(split.at(2)) - 1);
				edges.push_back(edge);

			} else if (split.front() == "clq")
			{
				VerticesArray clique;
				for (size_t i = 1; i < split.size(); ++i)
					clique.push_back(static_cast<size_t>(std::stol(split.at(i))) - 1);

				cliques.AddClique(clique);
			}
		}

		assert(edges.size() == edgesCount);

		return Dataset(graphName, verticesCount, edges, cliques);
	}
}
