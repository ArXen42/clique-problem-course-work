#pragma once

#include <stdint-gcc.h>
#include <vector>
#include <fstream>
#include "CliquesContainer.hpp"

namespace CliqueProblem
{
	typedef std::pair<size_t, size_t> GraphEdge;

	class Dataset
	{
	public:
		static Dataset ReadFrom(std::ifstream& stream);

	public:
		Dataset(std::string graphName, size_t verticesCount, std::vector<GraphEdge> const& edges, CliquesContainer const& cliques);

		const std::string GraphName_;
		const size_t VerticesCount_;
		const std::vector<GraphEdge> Edges_;
		const CliquesContainer Cliques_;
	};
}