#pragma once

#include <set>
#include "Dataset.hpp"

/// Представляет граф, реализованный списками смежности.
namespace CliqueProblem
{
	typedef std::set<size_t> VerticesSet;

	class Graph
	{
	public:
		Graph();
		Graph(size_t initialVertexCount);
		Graph(Dataset const& dataset);

		size_t VerticesCount() const { return adjacencyLists_.size(); }
		VerticesSet const& AdjacentVertices(size_t vertex) const { return adjacencyLists_.at(vertex); }
		bool AreVerticesConnected(size_t v1, size_t v2) const { return AdjacentVertices(v1).find(v2) != AdjacentVertices(v1).end(); }

		void AddVertices(size_t count);
		void AddVertex() { AddVertices(1); }
		void SetVerticesAdjacent(size_t vertex1, size_t vertex2);

	private:
		std::vector<VerticesSet> adjacencyLists_;
	};
}