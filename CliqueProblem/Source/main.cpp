#include <fstream>
#include <chrono>
#include <iostream>
#include <numeric>
#include <cmath>

#define DISABLE_CLIQUES_SAVE
#define ENABLE_TIME_BOUNDING
auto const MAX_ALGORITHM_RUNNING_TIME = std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(std::chrono::milliseconds(5000));

#include "CliqueProblemLibrary/Dataset.hpp"
#include "CliqueProblemLibrary/Algorithms/Algorithms.hpp"

template<typename BronKerboschStrategy>
void WriteAlgorithmRunResult(CliqueProblem::Dataset const& dataset, std::ostream& output, size_t runsCount = 5)
{
	using namespace CliqueProblem;
	typedef std::chrono::milliseconds::rep milliseconds_t;

	bool timeLimitExceed = false;
	std::vector<double> milliseconds(runsCount);
	std::vector<double> cliquesFound(runsCount);

	Graph graph(dataset);

	for (int i = 0; i < runsCount; ++i)
	{
		auto const& bronKerboschResult = BronKerbosch<BronKerboschStrategy>(graph);

		if (bronKerboschResult.TimeLimitExceed_)
		{
			if (!timeLimitExceed && i > 0)
				output << "Warning: some runs have exceed time limit and some have not." << std::endl;

			timeLimitExceed = true;
		} else
		{
			if (timeLimitExceed)
				output << "Warning: some runs have exceed time limit and some have not." << std::endl;
		}

		milliseconds[i] = bronKerboschResult.AlgorithmRunningTimeMilliseconds().count();
		cliquesFound[i] = bronKerboschResult.NonSavedCliquesFound_;
	}


	milliseconds_t averageMilliseconds = std::accumulate(milliseconds.begin(), milliseconds.end(), 0u)/runsCount;
	for (size_t i = 0; i < runsCount; ++i)
	{
		milliseconds[i] = milliseconds[i] - averageMilliseconds;
		milliseconds[i] *= milliseconds[i];
	}
	double millisecondsDeviation = std::sqrt(std::accumulate(milliseconds.begin(), milliseconds.end(), 0.)/(runsCount - 1));
	millisecondsDeviation /= averageMilliseconds;

	size_t averageCliquesFound = std::accumulate(cliquesFound.begin(), cliquesFound.end(), 0u)/runsCount;
	for (size_t i = 0; i < runsCount; ++i)
	{
		cliquesFound[i] -= averageCliquesFound;
		cliquesFound[i] *= cliquesFound[i];
	}
	double cliquesFoundDeviation = std::sqrt(std::accumulate(cliquesFound.begin(), cliquesFound.end(), 0.)/(runsCount - 1));
	cliquesFoundDeviation /= averageCliquesFound;

	const char separator = '	';
	output << dataset.GraphName_ << separator
	       << BronKerboschStrategy::AlgorithmName() << separator
	       << (timeLimitExceed ? "Да" : "Нет") << separator
	       << averageMilliseconds << separator
	       << millisecondsDeviation << separator
	       << averageCliquesFound
	       << separator << cliquesFoundDeviation
	       << std::endl;
}

void WriteAllAlgorithmsRunResult(CliqueProblem::Dataset const& dataset, std::ostream& output)
{
	using namespace CliqueProblem;

	WriteAlgorithmRunResult<BronKerboschOriginal>(dataset, output);
	WriteAlgorithmRunResult<BronKerboschPivot<MaxDegreePivotStrategy>>(dataset, output);
	WriteAlgorithmRunResult<BronKerboschPivot<RandomPivotStrategy>>(dataset, output);
	WriteAlgorithmRunResult<BronKerboschDegeneracy<MaxDegreePivotStrategy>>(dataset, output);
	WriteAlgorithmRunResult<BronKerboschDegeneracy<RandomPivotStrategy>>(dataset, output);
}

int main(int argc, char** argv)
{
	using namespace CliqueProblem;
	srand(time(0));

	std::ofstream ofs("output.txt");
	for (int i = 1; i < argc; ++i)
	{
		char* arg = argv[i];
		std::ifstream ifs(arg);

		if (!ifs.is_open())
			throw std::invalid_argument("Can't open file \"" + std::string(arg) + "\".");

		Dataset dataset = Dataset::ReadFrom(ifs);

		WriteAllAlgorithmsRunResult(dataset, ofs);
	}

	return EXIT_SUCCESS;
}
