#include <fstream>
#include "gtest/gtest.h"
#include "../../Source/CliqueProblemLibrary/Algorithms/Algorithms.hpp"

using namespace CliqueProblem;

class BronKerboschTests : public testing::TestWithParam<std::string>
{
public:
	static void SetUpTestCase()
	{
		srand(time(0));
	}

protected:
	template<typename BronKerboschStrategy>
	static bool AllMaximalCliquesFound(Dataset const& dataset)
	{
		CliquesContainer const& result = BronKerbosch<BronKerboschStrategy>(Graph(dataset));

		return result == dataset.Cliques_;
	}

	static Dataset LoadDataset(std::string path)
	{
		std::ifstream istream(path);
		return Dataset::ReadFrom(istream);
	}
};

TEST_P(BronKerboschTests, Original)
{
	auto dataset = LoadDataset(GetParam());
	ASSERT_TRUE(
			AllMaximalCliquesFound<BronKerboschOriginal>(dataset)
	);
}


TEST_P(BronKerboschTests, PivotGPX)
{
	auto dataset = LoadDataset(GetParam());
	ASSERT_TRUE(
			AllMaximalCliquesFound<BronKerboschPivot<MaxDegreePivotStrategy>>(dataset)
	);
}

TEST_P(BronKerboschTests, PivotGRX)
{
	auto dataset = LoadDataset(GetParam());
	ASSERT_TRUE(
			AllMaximalCliquesFound<BronKerboschPivot<RandomPivotStrategy>>(dataset)
	);
}


TEST_P(BronKerboschTests, DegeneracyOrderCorrect)
{
	auto dataset = LoadDataset(GetParam());
	auto graph = Graph(dataset);

	VerticesSet vertices;
	for (size_t i = 0; i < graph.VerticesCount(); ++i)
		vertices.insert(i);

	auto degeneracyOrder = BronKerboschDegeneracy<MaxDegreePivotStrategy>::GetDegeneracyOrder(vertices, graph);

	auto it = degeneracyOrder.OrderedVertices_.begin();
	while (it != degeneracyOrder.OrderedVertices_.end())
	{
		size_t vertex = *it;
		size_t neighboursCount = 0;

		auto it2 = it;
		it2++;
		while (it2 != degeneracyOrder.OrderedVertices_.end())
		{
			if (graph.AreVerticesConnected(vertex, *it2))
				neighboursCount++;
			++it2;
		}

		ASSERT_LE(neighboursCount, degeneracyOrder.Degeneracy_);
		++it;
	}
}

TEST_P(BronKerboschTests, DegeneracyGPX)
{
	auto dataset = LoadDataset(GetParam());
	ASSERT_TRUE(
			AllMaximalCliquesFound<BronKerboschDegeneracy<MaxDegreePivotStrategy>>(dataset)
	);
}

TEST_P(BronKerboschTests, DegeneracyGRX)
{
	auto dataset = LoadDataset(GetParam());
	ASSERT_TRUE(
			AllMaximalCliquesFound<BronKerboschDegeneracy<RandomPivotStrategy>>(dataset)
	);
}


INSTANTIATE_TEST_CASE_P(
		InstantiationOnTestingDatasets,
		BronKerboschTests,
		testing::Values(
				"Datasets/ValidationsDatasets/small-graph-1.dts",
				"Datasets/ValidationsDatasets/small-graph-1-with-isolated-vertices.dts",
				"Datasets/ValidationsDatasets/small-graph-2.dts",
				"Datasets/ValidationsDatasets/small-graph-3.dts"
		));
