include_directories(${gtest_SOURCE_DIR}/include {gtest_SOURCE_DIR})

add_executable(RunValidations
        BronKerboschTests.cpp)

target_link_libraries(RunValidations gtest gtest_main)
target_link_libraries(RunValidations CliqueProblemLibrary)

set(DATASETS_LOCATION "${CMAKE_SOURCE_DIR}/Datasets")
set(DATASETS_DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/Datasets")

add_custom_command(
        TARGET RunValidations POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${DATASETS_LOCATION} ${DATASETS_DESTINATION}
        DEPENDS ${DATASETS_DESTINATION}
        COMMENT "symbolic link datasets directory from ${DATASETS_LOCATION} => ${DATASETS_DESTINATION}"
)
