\documentclass[12pt, a4paper]{article}
\usepackage{xltxtra}

\setromanfont{Liberation Sans}
\setsansfont{Liberation Sans}
\setmonofont{DejaVu Sans Mono}

\usepackage{polyglossia}
\setmainlanguage{russian}
\setotherlanguage{english}
\PolyglossiaSetup{russian}{indentfirst=true}
\parindent=1.25cm

\usepackage {setspace}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{newfloat}

\usepackage[doi=false, url=false, sorting=ynt]{biblatex}
\addbibresource{sources.bib}

\usepackage[outputdir=\detokenize{../out/}, newfloat=true]{minted}
\setminted[text]{
	linenos=true,
	breaklines=true,
	encoding=utf8,
	frame=single,
	tabsize = 3
}
\setminted[c++]{
	linenos=true,
	breaklines=true,
	encoding=utf8,
	frame=single,
	tabsize = 3,
	fontsize=\small
}

\SetupFloatingEnvironment{listing}{name=Листинг}

\usepackage{graphicx}
\DeclareGraphicsExtensions{.pdf,.png}
\graphicspath{{images/pdf/}{images/png/}}
\usepackage{pdfpages}

\usepackage{geometry}
\geometry{verbose,a4paper,tmargin=2cm,bmargin=2cm,lmargin=3.5cm,rmargin=1cm}

\usepackage{titlesec}
\newcommand{\sectionbreak}{\clearpage}
\newcommand{\anonsection}[1]{\section*{#1}\addcontentsline{toc}{section}{#1}}

\onehalfspacing
\hyphenpenalty=10000
\tolerance=1
\emergencystretch=1000

\usepackage{multirow}

\renewcommand\labelitemi{--}

\begin{document}
	\includepdf{images/pdf/title-page.pdf}

	\tableofcontents
	\pagebreak

	\begin{anonsection}{Введение}

		Рассматриваемая в данной работе задача заключается в поиске максимальных клик заданного графа, т.е. его полных подграфов максимального размера. Рассмотрены существующие алгоритмы и проведено сравнение их эффективности.

		Возможность искать клики может оказаться полезной в задачах анализа данных: анализ коммуникационных сетей, электрических цепей, поиск групп знакомых людей в социальных сетях и других. Кроме того, клики широко применяются в биоинформатике и вычислительной химии.

	\end{anonsection}

	\begin{section}{Обзор задачи и алгоритмов}

		\textbf{Кликой} графа \(G\) называется полный подграф графа \(G\).

		\textbf{Максимальной кликой} графа \(G\) называется такая его клика, что ее невозможно расширить, т.е. добавить смежную вершину.

		\textbf{Наибольшей кликой} графа \(G\) называется его клика, содержащая максимально возможное число вершин.

		Из всего многообразия задач, связанных с кликами, в данной работе рассматривается задача поиска максимальных клик в заданном графе. Она относится к NP-трудным~\cite{cormen}, поэтому кроме алгоритмов, определяющих точный список максимальных клик, на больших графах зачастую используются более быстрые алгоритмы, работающие за полиномиальное время, но возвращающие только приблизительный результат.

		Далее приведены описания рассматриваемых алгоритмов и теоретическая оценка их сложности. Реализация основных модификаций алгоритма Брона--Кербоша приведена в раделе \ref{implementation}, экспериментальное сравнение их эффективности в разделе \ref{comparison}.

		\begin{subsection}{Полный перебор}

			Алгоритм заключается в проверке на клику всех возможных подграфов данного графа.

			Его верхняя граница сложности, соответственно, вычисляется как сумма произведений количества подграфов размера \(i\) и сложности проверки каждого из этих подграфов на полноту (проверка смежности каждой вершины с каждой):
			\begin{equation}
				\sum_{i = 1}^{n} ( C_n ^i * i^2),
			\end{equation}
			где n - количество вершин в графе.

			Вычисляя сумму, получаем сложность
			\begin{equation}
			    O(2^n n^2)
			\end{equation}

			Ввиду столь высокой вычислительной сложности, данный алгоритм применим только для небольших графов.

			Для получения списка максимальных клик, из полученного множества клик нужно выбрать только максимальные, то есть убрать клики, являющиеся подмножествами других, что еще больше замедляет алгоритм.

		\end{subsection}

		\begin{subsection}{Алгоритм Брона -- Кербоша}
		\label{bron-kerbosch-overview}

			Реализация метода ветвей и границ для поиска всех клик графа. Наиболее широко используемый и универсальный алгоритм.
			Алгоритм имеет вычислительную сложность \(O(3^{n/3})\), показанную Tomita, Tanaka и Haruhisa в 2006~\cite{bron-kerbosch-worst-case}.

			Его авторы и другие исследователи впоследствии также создали несколько его модификаций, показывающих себя лучше на определенных типах графов. Некоторые из этих модификаций будут рассмотрены далее.

			\begin{subsubsection}{Оригинальный алгоритм}

				Оригинальная версия алгоритма разработана голландскими математиками Броном и Кербошем в 1973 году~\cite{bron-kerbosch-1973}.

				Его псевдоход приведен в листинге \ref{bk-orig-pseudo}.
				\begin{listing}[h]
					\inputminted{text}{listings/bk-orig-pseudo.txt}
					\caption{Алгоритм Брона--Кербоша.}
					\label{bk-orig-pseudo}
				\end{listing}

				Здесь \(R, P, X\) - множества вершин графа, функция \(N(v)\) возвращает список смежных вершин \(v\). \(R\) и \(X\) изначально пустые, \(P\) содержит все вершины графа.
			\end{subsubsection}

			\clearpage
			\begin{subsubsection}{Модификация с использованием опорной вершины (pivot vertex)}
				Данная модификация позволяет сократить количество рекурсивных вызовов в графах, имеющих много немаксимальных клик. Изначальный ее вариант был описан авторами оригинального алгоритма как <<Version 2>>~\cite{bron-kerbosch-1973}.

				Перед итерацией по множеству P определенным образом выбирается опорная вершина \(u\), итерация проводится только по тем вершинам P, которые не являются смежными \(u\) (см. Листинг \ref{bk-pivot-pseudo}).

				\begin{listing}[h]
					\inputminted{text}{listings/bk-pivot-pseudo.txt}
					\caption{Алгоритм, использующий опорную вершину.}
					\label{bk-pivot-pseudo}
				\end{listing}

				В исходном варианте были описаны стратегии выбора \(u\) из \(P\):
				\begin{itemize}[itemsep=0]
					\item Выбирается случайная вершина;
					\item Выбирается вершина наибольшей степени;
				\end{itemize}

				\citeauthor{bron-kerbosch-pivot} в своей работе ~\citetitle{bron-kerbosch-pivot}~\cite{bron-kerbosch-pivot} показали, что эта вершина может быть выбрана из \(P \bigcup X\) без потери возможных максимальных клик, а также привели улучшенные стратегии по ее выбору.
			\end{subsubsection}

			\clearpage
			\begin{subsubsection}{Модификация с упорядочиванием вершин}
				Данный алгоритм описывается в статье \citetitle{eppstein-strash-2010}~\cite{eppstein-strash-2010} и является дальнейшим развитием предыдущего. Результаты его экспериментального исследования были опубликованы в их более поздней статье \cite{eppstein-strash-2013}.

				Псевдокод алгоритма приведен в листинге \ref{bk-degeneracy-pseudo}.

				\begin{listing}[h]
					\inputminted{text}{listings/bk-degeneracy-pseudo.txt}
					\caption{Алгоритм Брона--Кербоша с упорядочиванием вершин.}
					\label{bk-degeneracy-pseudo}
				\end{listing}

				Пусть G является k-вырожденным графом. Вершины упорядочиваются таким образом, что каждая вершина имеет k или меньше соседей, идущих дальше в порядке.

				\citeauthor{MatulaBeck} описали алгоритм, позволяющий найти такое упорядочивание за линейное время \cite{MatulaBeck}. Он заключается в последовательном удалении из графа вершин минимальной степени и вставки их в результирующий список.

				Данный алгоритм показывает хорошие результаты на разреженных графах. Упорядочивание вершин графа в приводит к дополнительной оптимизации количества рекурсивных вызовов.
			\end{subsubsection}

		\end{subsection}

		\begin{subsection}{Другие алгоритмы}
			Кроме вариаций алгоритмов Брона--Кербоша, существуют специализированные алгоритмы, показывающие себя лучше на определенных видах графов. Например, существует алгоритм, в основе которого лежит быстрое перемножение матриц, имеющий полиномиальное время на одну клику~\cite{Makino2004}. Сложность этого алгоритма зависит от его вывода (англ. output-sensitive), поэтому он эффективен на графах с небольшим количеством максимальных клик.

			Uriel Feige разработал аппроксимационный алгоритм~\cite{Feige2005}, работающий за полиномиальное время.
		\end{subsection}

	\end{section}

	\begin{section}{Программная реализация}
	\label{implementation}

		Реализованы и протестированы варианты алгоритма Брона--Кербоша, описанные в разделе \ref{bron-kerbosch-overview}.

		Для разработки был выбран язык C++. Сборка осуществлялась с помощью системы CMake, работоспособность проверена для компилятора gcc. Для тестирования использовался Google Testing Framework.

		Все исходные файлы проекта доступны в репозитории, указанном в заключении.

		\begin{subsection}{Граф}
			Граф реализован списками смежности. Так как в алгоритмах активно используется операция пересечения множеств, для ее оптимизации и упрощения реализации, списки смежности реализованы структурой данных std::set, основанной на сбалансированных деревьях.

			Его интерфейс приведен в листинге \ref{Graph.hpp}, реализация методов~--- в листинге \ref{Graph.cpp}.
			\begin{listing}[!h]
				\inputminted{c++}{listings/Graph.hpp}
				\caption{Интерфейс графа.}
				\label{Graph.hpp}
			\end{listing}
			\begin{listing}[h]
				\inputminted{c++}{listings/Graph.cpp}
				\caption{Методы графа.}
				\label{Graph.cpp}
			\end{listing}
		\end{subsection}

		\clearpage
		\begin{subsection}{Построение пересечения множеств}
			Изначально для построения пересечения множеств использовался алгоритм std::set\_intersection, но его использование было прекращено в пользу чуть более быстрой простой собственной реализации, представленной в листинге \ref{BronKerboschCommon}.

			\begin{listing}[h]
				\inputminted{c++}{listings/BronKerboschCommon.hpp}
				\caption{Реализация получения пересечения множеств.}
				\label{BronKerboschCommon}
			\end{listing}
		\end{subsection}

		\begin{subsection}{Инициализация алгоритмов}
			Все три модификации алгоритма представлены одной функцией \texttt{BronKerbosch}, приведенной в листинге \ref{BronKerboschInit}. Она инициализирует множества \texttt{R, P, X} и параметризована шаблонным агрументом \texttt{Strategy}, представляющим конкретную модификацию алгоритма. В свою очередь, две последние модификации также параметризуются алгоритмом выбора опорной вершины.

			\begin{listing}[h]
				\inputminted{c++}{listings/BronKerboschInit.hpp}
				\caption{Инициализация алгоритма Брона-Кербоша.}
				\label{BronKerboschInit}
			\end{listing}
		\end{subsection}

		\clearpage
		\begin{subsection}{Оригинальная версия алгоритма Брона--Кербоша}
			В целом, реализация данной функции максимально приближена к псевдокоду в листинге \ref{bk-orig-pseudo}.

			Построение пересечений множеств \texttt{P}, \texttt{X} со множеством соседей \texttt{v} и передача их в рекурсивный вызов требует создания новых объектов множеств, но передача объединения \(R \bigcup \{v\}\) реализована вставкой \texttt{v} перед рекурсивным вызовом и удалением после него.

			\begin{listing}[h]
				\inputminted{c++}{listings/BronKerboschOriginal.hpp}
				\caption{Реализация оригинального алгоритма Брона--Кербоша.}
				\label{BronKerboschOriginal}
			\end{listing}
		\end{subsection}

		\clearpage
		\begin{subsection}{Модификация с использованием опорной вершины}
			Реализация алгоритма приведена в листинге \ref{BronKerboschPivot}.
			Две реализованные реализации выбора опорной вершины приведены в листингах \ref{MaxDegreePivotStrategy} и \ref{RandomPivotStrategy}. К сожалению, реализация std::set не предоставляет произвольный доступ к его элементам, поэтому получение случайного элемента хоть и достаточно быстро, но все же имеет неконстантную сложность.

			\begin{listing}[h]
				\inputminted{c++}{listings/BronKerboschPivot.hpp}
				\caption{Реализация алгоритма Брона--Кербоша с выбором опорной вершины.}
				\label{BronKerboschPivot}
			\end{listing}

			\begin{listing}[h]
				\inputminted{c++}{listings/MaxDegreePivotStrategy.hpp}
				\caption{Выбор опорной вершины максимальной степени.}
				\label{MaxDegreePivotStrategy}
			\end{listing}
			\begin{listing}[h]
				\inputminted{c++}{listings/RandomPivotStrategy.hpp}
				\caption{Выбор случайной опорной вершины.}
				\label{RandomPivotStrategy}
			\end{listing}
		\end{subsection}

		\clearpage
		\begin{subsection}{Модификация с упорядочиванием вершин}
			Реализация алгоритма приведена в листине \ref{BronKerboschDegeneracy}, алгоритма упорядочивания вершин --- в листинге \ref{DegeneracyOrder}.

			\begin{listing}[h]
				\inputminted{c++}{listings/BronKerboschDegeneracy.hpp}
				\caption{Реализация алгоритма Брона--Кербоша с упорядочиванием вершин.}
				\label{BronKerboschDegeneracy}
			\end{listing}
			\begin{listing}[h]
				\inputminted{c++}{listings/DegeneracyOrder.hpp}
				\caption{Алгоритм упорядочивания вершин.}
				\label{DegeneracyOrder}
			\end{listing}
		\end{subsection}

		\clearpage
		\begin{subsection}{Тестирование корректности}
			Для проверки корректности реализации алгоритмов, был написан набор тестов, сопоставляющий результат их выполнения на заданном графе с заранее известными для него кликами. Также проверялась корректность работы алгоритма упорядочивания вершин.

			\noindent
			\includegraphics[width=\linewidth]{tests}
		\end{subsection}

	\end{section}

	\begin{section}{Экспериментальное сравнение}
	\label{comparison}

		\begin{subsection}{Методика сравнения}
			Для экспериментального сравнения были взяты несколько семейств графов, использовавшихся в соревновании <<Second DIMACS Implementation Challenge (1992-1993)>>.

			Графы одного семейства отличаются размерами, но обладают сходными свойствами.

			Для получения результатов на больших графах, приведенные ранее алгоритмы были модифицированы следующим образом:

			\begin{itemize}[itemsep = 0]
				\item В алгоритмах отключено сохранение найденных клик: в больших графах могут быть сотни тысяч максимальных клик (в отличие от более узкой категории наибольших клик), что приводит к большим затратам памяти и времени на их сохранение. Проблема сохранения результата выходит за рамки тестирования непосредственно алгоритма, поэтому вместо копирования найденной клики в контейнер производится простой инкремент счетчика, позволяющего определить общее найденное количество максимальных клик;

				\item Введено ограничение по времени: выполнение алгоритма останавливается после превышения лимита в 5 секунд. В таком случае сравнивается количество найденных за это время клик;
			\end{itemize}

			Изменения проведены с помощью условной компиляции, поэтому они не затрагивают тесты на корректность.

			На каждом из графов каждый алгоритм запускался по пять раз, после чего вычисляются усредненные результаты и их разброс (среднеквадратическое отклонение).

			Результаты затем группируются по семействам и представлены в таблицах в следующих разделах. В таблицах использованы условные обозначения GPX и GRX. Они, как и в работе \citeauthor{bron-kerbosch-pivot}~\cite{bron-kerbosch-pivot}, обозначают выбор опорной вершины максимальной степени и выбор случайной вершины соответственно.

			Во многих случаях алгоритмы не смогли найти все максимальные клики за отведенное время (пять секунд). Их сравнение производилось по количеству клик, которых они успели найти.
		\end{subsection}

		\clearpage
		\begin{subsection}{Семейство broсk}

			Результаты тестирования алгоритмов приведены в таблице \ref{brock}. Описание данного семейства графов (здесь и далее: данные графы и другая информация доступны по адресу \url{http://iridia.ulb.ac.be/~fmascia/maximum_clique}):
			\begin{quote}
				Random graphs with cliques hidden among nodes that have a relatively low degree. Instances generated by Mark Brockington and Joe Culberson.
			\end{quote}

			Первое число в имени графа обозначает количество вершин. Ребер у графов данного семейства достаточно много, в десятки или сотни раз больше количества вершин, поэтому во всех случаях, кроме \texttt{brock200\_2.b} (самое маленькое количество ребер, 9876) за отведенное время все максимальные клики алгоритмы найти не успели. У \verb|brock400_3.b| ребер 59681.

			Как и для других семейств, базовая версия алгоритма показала в разы меньшую эффективность по сравнению с остальными.

			Во всех случаях стратегия выбора случайной опорной точки себя не оправдала: это соотносится с результатами, описанными \citeauthor{bron-kerbosch-pivot}~\cite{bron-kerbosch-pivot}. Стоит также отметить, что низкая производительность этой стратегии в моей реализации также сыграла свою роль.

			На многих графах дополнительная сортировка вершин перед вызовом основного алгоритма проиграла алгоритмам без нее. Это закономерно, так как эти графы сравнительно плотные, в то время как данная оптимизация направлена в первую очередь на разреженные графы.

			Низкий разброс количества найденных клик и затраченного времени показывает, что полученные результаты для каждого графа и алгоритма стабильны. Разброс в измерениях для других графов также оказался мал, поэтому приводиться далее не будет.

			\noindent
			\begin{table}[h]
				\centering
				\begin{tabular}{|l|l|l|l|l|l|l|}
				\hline
				Граф                           & Алгоритм      & \begin{tabular}[c]{@{}l@{}}Время\\ превыш.\end{tabular} & \begin{tabular}[c]{@{}l@{}}Время\\ (мс)\end{tabular} & \begin{tabular}[c]{@{}l@{}}max\\ клик\end{tabular} & \begin{tabular}[c]{@{}l@{}}Разброс\\ (отн.)\end{tabular} & \begin{tabular}[c]{@{}l@{}}Разброс\\ времени\\  (отн.)\end{tabular} \\ \hline
				\multirow{5}{*}{brock200\_1.b} & Original      & Да                                                      & 5005                                                 & 54,188                                             & 0.00133                                                  &                                                                     \\ \cline{2-7}
				& PivotGPX      & Да                                                      & 5001                                                 & 937,523                                            & 0.00045                                                  &                                                                     \\ \cline{2-7}
				& PivotGRX      & Да                                                      & 5001                                                 & 634,021                                            & 0.10036                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGPX & Да                                                      & 5003                                                 & 930,527                                            & 0.00183                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGRX & Да                                                      & 5003                                                 & 604,216                                            & 0.08220                                                  &                                                                     \\ \hline
				&               &                                                         &                                                      &                                                    &                                                          &                                                                     \\ \hline
				\multirow{5}{*}{brock200\_2.b} & Original      & Да                                                      & 5001                                                 & 429,584                                            & 0.00052                                                  &                                                                     \\ \cline{2-7}
				& PivotGPX      & Нет                                                     & 1781                                                 & 431,586                                            & 0.00000                                                  & 0.00069                                                             \\ \cline{2-7}
				& PivotGRX      & Нет                                                     & 2047                                                 & 431,586                                            & 0.00000                                                  & 0.01612                                                             \\ \cline{2-7}
				& DegeneracyGPX & Нет                                                     & 1978                                                 & 431,586                                            & 0.00000                                                  & 0.00000                                                             \\ \cline{2-7}
				& DegeneracyGRX & Нет                                                     & 2215                                                 & 431,586                                            & 0.00000                                                  & 0.00161                                                             \\ \hline
				&               &                                                         &                                                      &                                                    &                                                          &                                                                     \\ \hline
				\multirow{5}{*}{brock200\_3.b} & Original      & Да                                                      & 5003                                                 & 230,727                                            & 0.00206                                                  &                                                                     \\ \cline{2-7}
				& PivotGPX      & Да                                                      & 5000                                                 & 968,143                                            & 0.00087                                                  &                                                                     \\ \cline{2-7}
				& PivotGRX      & Да                                                      & 5001                                                 & 867,720                                            & 0.01941                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGPX & Да                                                      & 5003                                                 & 973,974                                            & 0.00026                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGRX & Да                                                      & 5003                                                 & 859,267                                            & 0.00834                                                  &                                                                     \\ \hline
				&               &                                                         &                                                      &                                                    &                                                          &                                                                     \\ \hline
				\multirow{5}{*}{brock400\_1.b} & Original      & Да                                                      & 5032                                                 & 44,005                                             & 0.00005                                                  &                                                                     \\ \cline{2-7}
				& PivotGPX      & Да                                                      & 5006                                                 & 668,598                                            & 0.00401                                                  &                                                                     \\ \cline{2-7}
				& PivotGRX      & Да                                                      & 5007                                                 & 554,197                                            & 0.04874                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGPX & Да                                                      & 5019                                                 & 749,756                                            & 0.00356                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGRX & Да                                                      & 5019                                                 & 587,762                                            & 0.04110                                                  &                                                                     \\ \hline
				&               &                                                         &                                                      &                                                    &                                                          &                                                                     \\ \hline
				\multirow{5}{*}{brock400\_2.b} & Original      & Да                                                      & 5030                                                 & 61,373                                             & 0.00167                                                  &                                                                     \\ \cline{2-7}
				& PivotGPX      & Да                                                      & 5006                                                 & 628,212                                            & 0.00032                                                  &                                                                     \\ \cline{2-7}
				& PivotGRX      & Да                                                      & 5007                                                 & 589,919                                            & 0.09051                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGPX & Да                                                      & 5018                                                 & 708,573                                            & 0.00055                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGRX & Да                                                      & 5019                                                 & 573,195                                            & 0.02704                                                  &                                                                     \\ \hline
				&               &                                                         &                                                      &                                                    &                                                          &                                                                     \\ \hline
				\multirow{5}{*}{brock400\_3.b} & Original      & Да                                                      & 5032                                                 & 70,142                                             & 0.00301                                                  &                                                                     \\ \cline{2-7}
				& PivotGPX      & Да                                                      & 5007                                                 & 567,588                                            & 0.00012                                                  &                                                                     \\ \cline{2-7}
				& PivotGRX      & Да                                                      & 5008                                                 & 611,919                                            & 0.07811                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGPX & Да                                                      & 5019                                                 & 645,388                                            & 0.00216                                                  &                                                                     \\ \cline{2-7}
				& DegeneracyGRX & Да                                                      & 5019                                                 & 573,172                                            & 0.06888                                                  &                                                                     \\ \hline
				\end{tabular}
				\caption{Семейство brock.}
				\label{brock}
			\end{table}
		\end{subsection}

		\clearpage
		\begin{subsection}{Семейство mann}
			Оригинальное описание:
			\begin{quote}
				Clique formulation of the Steiner Triple Problem, translated from the set covering formulation. Instances generated by Carlo Mannino.
			\end{quote}

			Для данного семейства графов разница в результатах разных алгоритмов оказалась особенно значительна (см таблицу \ref{mann}).

			Базовая версия оказалась практически неработоспособной на данных графах.

			Выбор случайной опороной вершины на данном семействе графов также показал низкие результаты, GP версия значительно эффективнее.

			Упорядочивание вершин улучшения результатов также не дало.

			\begin{table}[h]
			\centering
				\begin{tabular}{|l|l|l|l|l|}
				\hline
				Граф                                                                                                  & Алгоритм      & Время \newline превышено & Время & Найдено max клик \\ \hline
				\multirow{5}{*}{\begin{tabular}[c]{@{}l@{}}MANN\_a9.clq.b\\ 45 вершин\\  918 ребер\end{tabular}}      & Original      & Да              & 5000  & 31359            \\ \cline{2-5}
				& PivotGPX      & Нет             & 800   & 590887           \\ \cline{2-5}
				& PivotGRX      & Нет             & 943   & 590887           \\ \cline{2-5}
				& DegeneracyGPX & Нет             & 964   & 590887           \\ \cline{2-5}
				& DegeneracyGRX & Нет             & 1095  & 590887           \\ \hline
				&               &                 &       &                  \\ \hline
				\multirow{5}{*}{\begin{tabular}[c]{@{}l@{}}MANN\_a27.clq.b \\ 378 вершин\\  70551 ребер\end{tabular}} & Original      & Да              & 5117  & 4                \\ \cline{2-5}
				& PivotGPX      & Да              & 5001  & 569005           \\ \cline{2-5}
				& PivotGRX      & Да              & 5002  & 132742           \\ \cline{2-5}
				& DegeneracyGPX & Да              & 5015  & 285803           \\ \cline{2-5}
				& DegeneracyGRX & Да              & 5015  & 156816           \\ \hline
				&               &                 &       &                  \\ \hline
				\multirow{5}{*}{\begin{tabular}[c]{@{}l@{}}MANN\_a45.clq.b\\ 1035 вершин\\ 533115 ребер\end{tabular}} & Original      & Да              & 7166  & 3                \\ \cline{2-5}
				& PivotGPX      & Да              & 5008  & 282057           \\ \cline{2-5}
				& PivotGRX      & Да              & 5013  & 30999            \\ \cline{2-5}
				& DegeneracyGPX & Да              & 5154  & 273613           \\ \cline{2-5}
				& DegeneracyGRX & Да              & 5153  & 26975            \\ \hline
				\end{tabular}
			\caption{Семейство mann.}
			\label{mann}
			\end{table}
		\end{subsection}

		\clearpage
		\begin{subsection}{Семейство gen}

			Оригинальное описание:
			\begin{quote}
				Artificially generated graphs with large, known embedded clique. Instances generated by Laura Sanchis.
			\end{quote}

			Количество вершин и ребер у первых двух графов в таблице \ref{gen}: 200 и 17910 соответственно.
			У следующих трех: 400 и 71820.

			В отличие от предыдущих семейств графов, алгоритмы \verb|*GRX| вели себя более нестабильно --- относительный разброс количества найденных максимальных клик для них составляет десятки процентов, но в целом их результат хуже \verb|GPX| версий.

			В отличие от предыдущих семейств, результаты алгоритма с упорядочиванием вершин здесь либо такие же, либо лучше чистого pivot--алгоритма (большое преимущество на графе \verb|gen400_p0.9_65.b|).

			Кроме описанных трех семейств, работа алгоритмов была протестирована на семействах \verb|C, hamming и p_hat|. Результаты в целом схожи с результатами для \verb|brock|. В некоторых случаях \verb|Degeneracy| версия опережала \verb|pivot|, но в большинстве случаев была отстающей.

			\begin{table}[h]
				\centering
					\begin{tabular}{|l|l|l|l|l|}
					\hline
					Граф                                                                                      & Алгоритм      & \begin{tabular}[c]{@{}l@{}}Время\\ превышено\end{tabular} & Время & \begin{tabular}[c]{@{}l@{}}Найдено\\ max клик\end{tabular} \\ \hline
					\multirow{5}{*}{\begin{tabular}[c]{@{}l@{}}gen200\_p0.9\_44.b\\ 17910 ребер\end{tabular}} & Original      & Да                                                        & 5017  & 2645                                                       \\ \cline{2-5}
					& PivotGPX      & Да                                                        & 5001  & 1364276                                                    \\ \cline{2-5}
					& PivotGRX      & Да                                                        & 5001  & 734905                                                     \\ \cline{2-5}
					& DegeneracyGPX & Да                                                        & 5004  & 1363540                                                    \\ \cline{2-5}
					& DegeneracyGRX & Да                                                        & 5004  & 677572                                                     \\ \hline
					&               &                                                           &       &                                                            \\ \hline
					\multirow{5}{*}{gen200\_p0.9\_55.b}                                                       & Original      & Да                                                        & 5016  & 3760                                                       \\ \cline{2-5}
					& PivotGPX      & Да                                                        & 5000  & 1080384                                                    \\ \cline{2-5}
					& PivotGRX      & Да                                                        & 5001  & 508425                                                     \\ \cline{2-5}
					& DegeneracyGPX & Да                                                        & 5004  & 1041478                                                    \\ \cline{2-5}
					& DegeneracyGRX & Да                                                        & 5004  & 473536                                                     \\ \hline
					&               &                                                           &       &                                                            \\ \hline
					\multirow{5}{*}{\begin{tabular}[c]{@{}l@{}}gen400\_p0.9\_55.b\\ 71820 ребер\end{tabular}} & Original      & Да                                                        & 5081  & 3018                                                       \\ \cline{2-5}
					& PivotGPX      & Да                                                        & 5006  & 882111                                                     \\ \cline{2-5}
					& PivotGRX      & Да                                                        & 5008  & 615094                                                     \\ \cline{2-5}
					& DegeneracyGPX & Да                                                        & 5022  & 880711                                                     \\ \cline{2-5}
					& DegeneracyGRX & Да                                                        & 5023  & 562909                                                     \\ \hline
					&               &                                                           &       &                                                            \\ \hline
					\multirow{5}{*}{gen400\_p0.9\_65.b}                                                       & Original      & Да                                                        & 5077  & 9166                                                       \\ \cline{2-5}
					& PivotGPX      & Да                                                        & 5005  & 566515                                                     \\ \cline{2-5}
					& PivotGRX      & Да                                                        & 5007  & 476910                                                     \\ \cline{2-5}
					& DegeneracyGPX & Да                                                        & 5022  & 1437886                                                    \\ \cline{2-5}
					& DegeneracyGRX & Да                                                        & 5024  & 448078                                                     \\ \hline
					&               &                                                           &       &                                                            \\ \hline
					\multirow{5}{*}{gen400\_p0.9\_75.b}                                                       & Original      & Да                                                        & 5091  & 10148                                                      \\ \cline{2-5}
					& PivotGPX      & Да                                                        & 5005  & 704548                                                     \\ \cline{2-5}
					& PivotGRX      & Да                                                        & 5008  & 460381                                                     \\ \cline{2-5}
					& DegeneracyGPX & Да                                                        & 5021  & 702861                                                     \\ \cline{2-5}
					& DegeneracyGRX & Да                                                        & 5024  & 671051                                                     \\ \hline
					\end{tabular}
				\caption{Семейство gen.}
				\label{gen}
			\end{table}
		\end{subsection}

	\end{section}

	\begin{anonsection}{Заключение}
		Был проведен обзор существующих алгоритмов нахождения максимальных клик в графах.

		Основные модификации алгоритма Брона--Кербоша были программно реализованы и протестированы на нескольких семействах графов. Наилучшие результаты с приведенной реализацией алгоритмов на протестированных семействах графов как правило показывала модификация с выбором вершины максимальной степени в качестве опорной точки.

		Тем не менее, для других реализаций и на других графах возможны другие результаты: например, упорядочивание вершин может дать большее преимущество на разреженных графах.

		Все исходные файлы данной курсовой: CMake/C++ проект, файлы графов и XeLaTeX текст данной работы доступны в репозитории по адресу \url{https://bitbucket.org/ArXen42/clique-problem-course-work/}.
	\end{anonsection}

	\printbibliography[title = Список использованных источников]

\end{document}
