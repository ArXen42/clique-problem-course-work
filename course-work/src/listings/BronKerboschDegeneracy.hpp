template<typename PivotSelectionStrategy>
struct BronKerboschDegeneracy
{
	static void ApplyStrategy(
		VerticesSet& R, VerticesSet& P, VerticesSet& X,
		Graph const& graph, CliquesContainer& cliquesContainer)
	{
		auto degeneracyOrder = GetDegeneracyOrder(P, graph);

		for (auto v: degeneracyOrder.OrderedVertices_)
		{
			auto& neighbours = graph.AdjacentVertices(v);

			VerticesSet Pi = SetIntersection(P, neighbours);
			VerticesSet Xi = SetIntersection(X, neighbours);

			R.insert(v);
			BronKerboschPivot<PivotSelectionStrategy>
				::ApplyStrategy(R, Pi, Xi, graph, cliquesContainer);
			R.erase(v);

			P.erase(v);
			X.insert(v);
		}
	}
};

