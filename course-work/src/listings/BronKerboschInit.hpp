template<typename Strategy>
CliquesContainer BronKerbosch(Graph const& graph)
{
	VerticesSet R, P, X;
	for (size_t i = 0; i < graph.VerticesCount(); ++i)
		P.insert(i);

	CliquesContainer result;
	Strategy::ApplyStrategy(R, P, X, graph, result);

	return result;
}
