struct BronKerboschOriginal
{
	static void ApplyStrategy
		(VerticesSet& R, VerticesSet& P, VerticesSet& X,
			Graph const& graph, CliquesContainer& cliquesContainer)
	{
		if (P.empty() && X.empty())
		{
			VerticesArray clique(R.begin(), R.end());
			cliquesContainer.AddClique(clique);
			return;
		}

		while (!P.empty())
		{
			size_t v = *P.begin();
			auto& neighbours = graph.AdjacentVertices(v);

			VerticesSet Pi = SetIntersection(P, neighbours);
			VerticesSet Xi = SetIntersection(X, neighbours);

			R.insert(v);
			ApplyStrategy(R, Pi, Xi, graph, cliquesContainer);
			R.erase(v);

			P.erase(v);
			X.insert(v);
		}
	}
};
