template<typename PivotSelectionStrategy>
struct BronKerboschPivot
{
	static void ApplyStrategy(
		VerticesSet& R, VerticesSet& P, VerticesSet& X,
		 Graph const& graph, CliquesContainer& cliquesContainer)
	{
		if (P.empty() && X.empty())
		{
			VerticesArray clique(R.begin(), R.end());
			cliquesContainer.AddClique(clique);
			return;
		}

		size_t pivot = PivotSelectionStrategy::GetPivot(P, X, graph);

		auto it = P.begin();
		while (it != P.end())
		{
			size_t v = *it;
			if (!graph.AreVerticesConnected(v, pivot))
			{
				auto& neighbours = graph.AdjacentVertices(v);

				VerticesSet Pi = SetIntersection(P, neighbours);
				VerticesSet Xi = SetIntersection(X, neighbours);

				R.insert(v);
				ApplyStrategy(R, Pi, Xi, graph, cliquesContainer);
				R.erase(v);

				it = P.erase(it);
				X.insert(v);
			} else
				++it;
		}
	}
};
