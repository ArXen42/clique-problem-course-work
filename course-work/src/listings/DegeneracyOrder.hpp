DegeneracyOrder GetDegeneracyOrder(VerticesSet& V, Graph& graph)
{
	DegeneracyOrder resultOrder;
	VerticesList& resultList = resultOrder.OrderedVertices_;
	size_t verticesCount = V.size();
	std::vector<VerticesList> D(verticesCount);
	std::vector<size_t> degrees(verticesCount);
	for (size_t v = 0; v < verticesCount; ++v)
	{
		size_t degree = graph.AdjacentVertices(v).size();
		degrees.at(v) = degree;
		D.at(degree).push_front(v);
	}

	size_t degeneracy = 0;
	for (size_t i = 0; i < verticesCount; ++i)
	{
		size_t firstNonEmptyDegree = 100;
		for (size_t degreeD = 0; degreeD < D.size(); ++degreeD)
		{
			if (!D.at(degreeD).empty())
			{
				firstNonEmptyDegree = degreeD;
				break;
			}
		}

		if (firstNonEmptyDegree > degeneracy)
			degeneracy = firstNonEmptyDegree;
		auto& vertices = D.at(firstNonEmptyDegree);

		size_t vertex = vertices.front();
		resultList.push_front(vertex);
		vertices.pop_front();

		auto& neighbours = graph.AdjacentVertices(vertex);
		for (size_t neighbour: neighbours)
			if (std::find(resultList.cbegin(), resultList.cend(), neighbour) == resultList.end())
			{
				size_t& degree = degrees.at(neighbour);

				D.at(degree).remove(neighbour);
				degree--;
				D.at(degree).push_front(neighbour);
			}
	}
	return resultOrder;
}
