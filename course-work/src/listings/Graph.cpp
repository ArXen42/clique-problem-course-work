Graph::Graph() : Graph(0) {}

Graph::Graph(size_t initialVertexCount)
{
	AddVertices(initialVertexCount);
}

Graph::Graph(Dataset const& dataset)
{
	AddVertices(dataset.VerticesCount_);
	for (auto& edge: dataset.Edges_)
		SetVerticesAdjacent(edge.first, edge.second);
}

void Graph::AddVertices(size_t count)
{
	for (size_t i = 0; i < count; i++)
		adjacencyLists_.push_back(VerticesSet());
}

void Graph::SetVerticesAdjacent(size_t vertex1, size_t vertex2)
{
	auto& vertex1List = adjacencyLists_.at(vertex1);
	auto& vertex2List = adjacencyLists_.at(vertex2);

	assert(std::find(vertex1List.begin(), vertex1List.end(), vertex2) == vertex1List.end());
	assert(std::find(vertex2List.begin(), vertex2List.end(), vertex1) == vertex2List.end());

	vertex1List.insert(vertex2);
	vertex2List.insert(vertex1);
}
