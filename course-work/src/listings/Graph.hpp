typedef std::set<size_t> VerticesSet;

/// Представляет граф, реализованный списками смежности.
class Graph
{
public:
	Graph();
	Graph(size_t initialVertexCount);
	Graph(Dataset const& dataset);

	size_t VerticesCount() const;
	VerticesSet const& AdjacentVertices(size_t vertex) const;
	bool AreVerticesConnected(size_t v1, size_t v2) const;

	void AddVertices(size_t count);
	void AddVertex() { AddVertices(1); }
	void SetVerticesAdjacent(size_t vertex1, size_t vertex2);

private:
	std::vector<VerticesSet> adjacencyLists_;
};
