struct MaxDegreePivotStrategy
{
	static size_t GetPivot(VerticesSet& P, VerticesSet& X, Graph const& graph)
	{
		size_t pivot;
		size_t maxDegree = 0;

		auto it = P.begin();
		while (it != X.end())
		{
			if (it == P.end())
				if (X.empty()) break;
				else it = X.begin();

			size_t vertex = *it;
			size_t degree = graph.AdjacentVertices(vertex).size();
			if (degree >= maxDegree)
			{
				pivot = vertex;
				maxDegree = degree;
			}

			it++;
		}

		assert(pivot < graph.VerticesCount());
		return pivot;
	}
};
