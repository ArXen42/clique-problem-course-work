struct RandomPivotStrategy
{
	static size_t GetPivot(VerticesSet& P, VerticesSet& X, Graph const& graph)
	{
		size_t verticesCount = P.size() + X.size();
		size_t pos = (rand()%verticesCount);

		VerticesSet::const_iterator it;

		if (pos < P.size())
		{
			it = P.begin();
			std::advance(it, pos);
		} else
		{
			it = X.begin();
			std::advance(it, pos - P.size());
		}

		return *it;
	}
};
