BronKerboschDegeneracy(G): //G - исходный граф
	P = V(G)
	R = X = empty
	for each vertex v in a degeneracy ordering of G:
		BronKerboschPivot(R ⋃ {v}, P ⋂ N(v), X ⋂ N(v))
		P := P \ {v}
		X := X ⋃ {v}
